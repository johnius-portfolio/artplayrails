class Company < ActiveRecord::Base
  include PgSearch

  has_many :images
  has_many :adverts
  has_and_belongs_to_many :tags

  pg_search_scope :search_by_name,
    :against => {
      :name => 'A'
    },
    :using => {
      :tsearch => {
        :prefix => true
      },
      :trigram => {
        :threshold => 0.15
      }
    },
    :order_within_rank => "companies.name DESC"
end
