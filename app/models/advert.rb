class Advert < ActiveRecord::Base
  belongs_to :company

  def to_jbuilder
    Jbuilder.new do |json|
      company = Company.find(self[:company_id])

      json.(self, :id, :filename, :company_id)
      json.title company[:name]
      json.text company[:preview_text]
      json.small small_version(self[:filename])
    end
  end

  def as_json
    to_jbuilder.attributes!
  end

  def small_version(filename)
    filename.gsub(/(.+)\.(.+)$/, '\1_small.\2')
  end
end
