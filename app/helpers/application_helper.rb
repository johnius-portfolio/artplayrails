module ApplicationHelper
  def tags_of_categories
    tags = []

    TagCategory.all.each do |category|
      tags << {name: category.name, tags: category.tags.select(:id, :name)}
    end

    tags.to_json
  end
end
