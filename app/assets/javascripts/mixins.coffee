@keepScrollPositionMixin =
  saveScrollPosition: (top) ->
    @pageState.set '_listScrollPosition', top

  getScrollPosition: ->
    @state.scrollPosition

  childContextTypes:
    saveScrollPosition: React.PropTypes.func
    scrollPosition: React.PropTypes.number

  getChildContext: ->
    saveScrollPosition: @saveScrollPosition
    scrollPosition: @getScrollPosition()
