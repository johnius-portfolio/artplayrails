#= require jquery
#= require jquery-ui
#= require underscore
#= require swiper
#= require keyboard
#= require iscroll

#= require classNames

#= require react
#= require react_ujs
#= require react-router
#= require baobab.min
#= require baobab-react-mixins
#= require imagesloaded.pkgd

#= require ./state
#= require ./mixins
#= require_tree ./components
#= require ./routes

#= require_self

# Disable dragging
$(document).on 'dragstart', -> false
