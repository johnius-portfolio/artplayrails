@Dummy = React.createClass
  displayName: 'Content'
  getDefaultProps: ->
    caption: null
  render: ->
    if @props.caption
      <div className="-no-results">
        {@props.caption}
      </div>

    else
      null
