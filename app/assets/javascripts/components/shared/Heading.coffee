@Heading = React.createClass
  displayName: 'Heading'
  contextTypes:
    router: React.PropTypes.func
  getDefaultProps: ->
    close: null
    simple: null
  render: ->
    noUnderline =
      borderBottom: 0

    <div className="heading" style={noUnderline if @props.simple}>
      <h1>{@props.children}</h1>
      {
        # TODO: Hide if cannot go back
        <div className="heading__close">
          <Close to={@props.close if @props.close} />
        </div> if @props.close
      }
    </div>
