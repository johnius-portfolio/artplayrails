@Gallery = React.createClass
  displayName: 'Gallery'
  getDefaultProps: ->
    images: []

  getInitialState: ->
    imagesLoaded: false

  componentDidMount: ->
    if @props.images.length
      $self = $(@getDOMNode())

    preview = $self.find("[role='gallery-preview']").swiper
      slidesPerView: 'auto'
      # keyboardControl: true
      centeredSlides: true
      spaceBetween: 1
      # slideToClickedSlide: true

    thumb = $self.find("[role='gallery-thumb']").swiper
      slidesPerView: 'auto'
      centeredSlides: true
      spaceBetween: 1
      slideToClickedSlide: true
      # freeMode: true

    preview.params.control = thumb
    thumb.params.control = preview

    if $self
      imagesLoaded $self.get(), =>
        @setState
          imagesLoaded: true
        preview.onResize()
        thumb.onResize()

  getDefaultProps: ->
    images: []

  render: ->
    renderPreviews = (slides) ->
      for index, image of slides
        styles =
          width: image.dims[0]
          height: image.dims[1]

        <div className="swiper-slide">
          <img src={image.url} width={styles.width} height={styles.height} />
        </div>

    renderThumbs = (slides) ->
      for index, image of slides
        styles =
          width: Math.ceil(image.dims[0] / image.dims[1] * 140)
          height: 140

        <div className="swiper-slide">
          <img src={image.url} width={styles.width} height={styles.height} />
        </div>

    <div className="gallery">
      <div className="gallery__preview">
        <div className="swiper-container" role="gallery-preview">
          <div className="swiper-wrapper">
            { renderPreviews @props.images }
          </div>
        </div>
      </div>

      <div className="gallery__thumb">
        <div className="swiper-container" role="gallery-thumb">
          <div className="swiper-wrapper">
            { renderThumbs @props.images }
          </div>
        </div>
      </div>

      { <LoadingSpinner stretch zindex="10" opacity="1" /> unless @state.imagesLoaded }
    </div>


# Sample pictures

# previewImages = []
# thumbImages = []

# for i in [1..10]
#   ratio = 1 + Math.random() * 0.75
#   previewImages.push "http://dummyimage.com/#{[Math.floor(480 * ratio), 480].join('x')}/999999/000000.jpg"
#   thumbImages.push "http://dummyimage.com/#{[Math.floor(140 * ratio), 140].join('x')}/999999/000000.jpg"
