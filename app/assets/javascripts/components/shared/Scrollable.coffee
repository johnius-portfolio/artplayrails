# Context
#  saveScrollPosition -- func
#  scrollPosition -- number
#  resetScrollPosition -- bool

@Scrollable = React.createClass
  displayName: 'Scrollable'
  contextTypes:
    saveScrollPosition: React.PropTypes.func
    scrollPosition: React.PropTypes.number

  propTypes:
    retreiveScrollInstance: React.PropTypes.func

  getDefaultProps: ->
    retreiveScrollInstance: ->

  componentDidUpdate: ->
    @props.retreiveScrollInstance @createScroll parseInt(@context.scrollPosition) or 0

  componentDidMount: ->
    @props.retreiveScrollInstance @createScroll parseInt(@context.scrollPosition) or 0

  componentWillUnmount: ->
    if _.isFunction @context.saveScrollPosition
      @context.saveScrollPosition @scroll.y

  createScroll: (y) ->
    @scroll = new IScroll @getDOMNode(),
      tap: true
      mouseWheel: true
      bounce: false
      startY: parseInt(y)
      scrollbars: 'custom'
      deceleration: 0.005

  scroll: null

  render: ->
    if @props.children
      <div className='scrollable'>
        <div>{@props.children}</div>
      </div>

    else
      null