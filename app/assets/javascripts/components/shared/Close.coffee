@Close = React.createClass
  displayName: 'Close'
  contextTypes:
    router: React.PropTypes.func
  getDefaultProps: ->
    to: null
    onClick: null
  handleClick: (e) ->
    e.preventDefault()

    if _.isFunction @props.onClick
      @props.onClick(e)
    else if @props.to
      @context.router.transitionTo @props.to
    else
      @context.router.goBack()

    return

  render: ->
    <div className="close" onClick={@handleClick}>
      <span />
    </div>
