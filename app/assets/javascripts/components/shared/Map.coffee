AppState = @AppState


# Facets

getMapSVG = AppState.createFacet
  cursors:
    svg: ['map', 'svg']

  get: (state) ->
    unless state.svg
      $.ajax '/static/Map.svg',
        dataType: 'text'
      .done (svg) ->
        AppState.select('map', 'svg').set svg.toString()

      null

    else
      state.svg


# Class

@Map = React.createClass
  displayName: 'Map'
  mixins: [BaobabReactMixins.branch]
  facets:
    svg: getMapSVG

  getDefaultProps: ->
    onChoose: null
    selected: null
    guide: null

  setMapEvents: ->
    $ document
      .off 'click', 'svg [id^="Bld"]'
      .on 'click', 'svg [id^="Bld"]', (e) =>
        if @isSelectable()
          @props.onChoose parseInt(e.currentTarget.id.slice 3)

  highlightBuilding: ->
    $map = $ @getDOMNode()
    $map
      .find 'svg [id^="Bld"]'
      .attr 'class', ''

    if id = @props.selected
      $map
        .find "svg #Bld#{id}"
        .attr 'class', 'active'

  componentDidMount: ->
    @setMapEvents()
    @highlightBuilding()

  componentDidUpdate: ->
    @highlightBuilding()

  isSelectable: ->
    _.isFunction @props.onChoose

  render: ->
    classes = classNames
      map: true
      "_selectable": @isSelectable()
      "_start-page": !!@props.guide

    <div className={classes}>
      {
        if @state.svg
          <span dangerouslySetInnerHTML={{__html: @state.svg}} />

        else
          <LoadingSpinner />
      }
    </div>
