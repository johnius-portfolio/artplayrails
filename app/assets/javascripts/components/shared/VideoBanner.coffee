# Data

adverts = window.adverts
currentBanner = @AppState.select('currentBanner')


# Class

@VideoBanner = React.createClass
  displayName: 'VideoBanner'
  mixins: [ReactRouter.Navigation, BaobabReactMixins.branch]
  cursors:
    currentBanner: ['currentBanner']

  getDefaultProps: ->
    full: false
    onClick: null

  componentDidMount: ->
    unless _.empty? adverts
      @video = @refs.video.getDOMNode()
      @video.addEventListener 'ended', @nextVideo
      @video.addEventListener 'click', (@props.onClick or @handleClick)

  shouldComponentUpdate: (prevProps, prevState) ->
    prevState.currentBanner != @state.currentBanner

  nextVideo: ->
    nextId = @state.currentBanner + 1
    nextId = 0 unless adverts[nextId]
    currentBanner.set nextId

  handleClick: (company_id) ->
    @transitionTo 'company_location', id: adverts[@state.currentBanner].company_id

  render: ->
    unless _.isEmpty adverts
      if @props.full
        filename = adverts[@state.currentBanner].filename
      else
        filename = adverts[@state.currentBanner].small

      classes = classNames
        "video-banner": true
        "_full": !!@props.full

      <div className={classes}>
        <video src="/videos/#{filename}" autoPlay loop={Object.keys(adverts).length == 1} ref="video" />
      </div>

    else
      null
