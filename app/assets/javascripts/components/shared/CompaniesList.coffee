@CompaniesList = React.createClass
  displayName: 'CompaniesList'

  getDefaultProps: ->
    companies: null
    msgNothing: '...'
    msgEmpty: 'No companies in this building yet'

  addPlaceholder: ->
    if placeholder = @getDOMNode().querySelector("#placeholder")
      placeholder.style.height = Math.floor((@getDOMNode().offsetHeight || 0) / 2) + 'px'
      @scroll.refresh()

  componentDidMount: ->
    @addPlaceholder()

  componentDidUpdate: ->
    @addPlaceholder()

  gotScroll: (scrollInstance) ->
    @scroll = scrollInstance

  render: ->
    <div className="companieslist">
      {
        companies = @props.companies

        if companies and companies.length
          <Scrollable retreiveScrollInstance={@gotScroll}>
            {
              for index, company of companies
                <CompanyLink name={company.name} logo={company.logo} id={company.id} key={company.id} />
            }
            <div className="companieslist__down-placeholder" id="placeholder" />
          </Scrollable>

        else if companies
          <Dummy caption={@props.msgEmpty} /> if @props.msgEmpty

        else
          <Dummy caption={@props.msgNothing} /> if @props.msgNothing
      }
    </div>
