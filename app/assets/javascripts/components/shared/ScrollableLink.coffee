@ScrollableLink = React.createClass
  displayName: 'ScrollableLink'
  mixins: [ReactRouter.Navigation]
  getDefaultProps: ->
    to: null
    params: {}
    query: {}
  componentDidMount: ->
    $ @getDOMNode()
      .on 'click', (e) ->
        e.preventDefault()
      .on 'tap', =>
        @transitionTo @props.to, @props.params, @props.query
  render: ->
    <Link {...@props}>
      {@props.children}
    </Link>
