@TagsCloud = React.createClass
  displayName: 'TagsCloud'
  getDefaultProps: ->
    tags: null
  render: ->
    unless @props.tags and _.isObject @props.tags and not _.isEmpty @props.tags
      <div className="tags-cloud">
        {
          for index, tag of @props.tags
            <div className="tags-cloud__tag">
              <Link className="tags-cloud__link" to="tag" params={{id: tag.id}}>
                {tag.name}
              </Link>
            </div>
        }
      </div>

    else
      null
