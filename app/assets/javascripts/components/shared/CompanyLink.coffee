@CompanyLink = React.createClass
  displayName: 'CompanyLink'

  getDefaultProps: ->
    logo: null
    name: null
    id: 0

  render: ->
    <div className='company-link'>
      <ScrollableLink className='company-link__link' to='company_location' params={{id: @props.id}}>
        <span dangerouslySetInnerHTML={{__html: @props.name.replace /[-–—]/g, '–'}} />
      </ScrollableLink>

      {
        if @props.logo
          <div className='company-link__logo'>
            <img src={@props.logo} />
          </div>
      }
    </div>
