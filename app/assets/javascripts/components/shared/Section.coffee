@Section = React.createClass
  displayName: 'Section'

  getDefaultProps: ->
    title: '...'
    close: null
    company: null

  render: ->
    classes = classNames
      'section': true
      '_company': @props.company

    <div className={classes}>
      <div className="section__title">{@props.title}</div>
      {
        if @props.close
          <div className="section__close">
            <Close onClick={@props.close} />
          </div>
      }
    </div>
