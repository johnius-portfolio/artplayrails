@Content = React.createClass
  displayName: 'Content'
  render: ->
    <div className="content">
      {@props.children}
    </div>
