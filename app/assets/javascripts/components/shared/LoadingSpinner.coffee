@LoadingSpinner = React.createClass
  displayName: 'LoadingSpinner'
  getDefaultProps: ->
    stretch: null
    zindex: null
    opacity: .5
  render: ->
    spinnerStyles =
      position: 'absolute'
      top: '50%'
      left: '50%'
      marginLeft: '-24px'
      marginTop: '-24px'
      display: 'block'
      pointerEvents: 'none'
      zIndex: @props.zindex != null && @props.zindex

    stretchContainerStyles =
      position: 'absolute'
      top: 0
      right: 0
      bottom: 0
      left: 0
      background: 'rgba(255, 255, 255, ' + @props.opacity + ')'
      zIndex: @props.zindex != null && @props.zindex

    <div style={if @props.stretch != null then stretchContainerStyles else spinnerStyles}>
      <div className="loading-spinner" style={spinnerStyles if @props.stretch} />
    </div>
