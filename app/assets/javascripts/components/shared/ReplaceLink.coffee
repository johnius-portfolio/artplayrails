@ReplaceLink = React.createClass
  displayName: 'ReplaceLink'
  mixins: [ReactRouter.Navigation]
  handleClick: (e) ->
    e.preventDefault()
    @replaceWith @props.to, @props.params

  render: ->
    <Link {...@props} onClick={@handleClick} />
