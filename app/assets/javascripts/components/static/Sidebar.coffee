# Data

adverts = window.adverts
currentBanner = @AppState.select('currentBanner')


# Class

@Sidebar = React.createClass
  displayName: 'Sidebar'
  mixins: [BaobabReactMixins.branch]
  cursors:
    currentBanner: ['currentBanner']

  contextTypes:
    router: React.PropTypes.func

  render: ->
    if Object.keys(adverts).length
      <div className="sidebar">
        <div className="sidebar__image" ref="banner">
          <VideoBanner />
        </div>
        <div className="sidebar__text">
          <h3 ref="title">{adverts[@state.currentBanner].title}</h3>
          <p ref="text" dangerouslySetInnerHTML={{__html: adverts[@state.currentBanner].text}} />
        </div>
      </div>

    else
      null
