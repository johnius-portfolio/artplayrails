# State

@AppState.select('sessions').set 'tags',
  pages: [0, 0]


# Data

pageState = @AppState.select('sessions', 'tags')

# Actions

# ...


# Class

@TagsPage = React.createClass
  displayName: 'TagsPage'
  contextTypes:
    router: React.PropTypes.func
  mixins: [ReactRouter.Navigation, BaobabReactMixins.branch]
  cursors:
    pages: ['sessions', 'tags', 'pages']
  nextPage: (e) ->
    e.preventDefault()
    column = e.target.getAttribute('data-index')

    pageState.apply 'pages', (current) ->
      ++current[column]

      return current
  prevPage: (e) ->
    e.preventDefault()
    column = e.target.getAttribute('data-index')

    pageState.apply 'pages', (current) ->
      --current[column]

      return current
  render: ->
    <div className="tags-page">
      <Heading>Search by service</Heading>
      <div className="tags-page__tags">
        {
          for category, index in categories
            perPage = 13
            offset = @state.pages[index] * perPage
            tags = _.first(_.rest(category.tags, offset), perPage)
            hasMore = !!category.tags[offset + perPage]
            canBack = !!category.tags[offset - 1]

            headingClasses = classNames
              "tags-page__heading": true
              "_orange": !(index % 2)
              "_purple": index % 2

            <div className="tags-page__column">
              <div className={headingClasses}>{category.name}</div>
              <div className="tags-page__items">
                {if canBack
                  <a href="#" className="tags-page__item _back" onClick={@prevPage} data-index={index}>Back</a>}
                {
                  for tag in tags
                    <Link className="tags-page__item" to="tag" params={{id: tag.id}}>
                      {tag.name}
                    </Link>
                }
                {if hasMore
                  <a href="#" className="tags-page__item" onClick={@nextPage} data-index={index}>More in ARTPLAY</a>}
              </div>
            </div>
        }
      </div>
    </div>
