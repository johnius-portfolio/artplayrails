# State

@AppState.select('sessions').set 'map',
  building: null
  companies: null
  _isLoading: false
  _listScrollPosition: null


# Data

pageState = @AppState.select('sessions', 'map')


# Actions

ajaxHandler = null

onBuildingChoose = (id) ->
  return if id == pageState.get('building')

  ajaxHandler.abort() unless _.isNull ajaxHandler

  pageState
    .set 'building', id
    .set '_isLoading', true
    .set '_listScrollPosition', null

  ajaxHandler =
    $.ajax
      url: "/building/#{id}"
      success: (response) ->
        pageState
          .set 'companies', response
          .set '_isLoading', false
          .set '_listScrollPosition', 0
      error: ->
        pageState.set '_isLoading', false


# Class

@MapPage = React.createClass
  displayName: 'MapPage'
  mixins: [BaobabReactMixins.branch, keepScrollPositionMixin]
  cursors:
    session: ['sessions', 'map']
    isLoading: ['sessions', 'map', '_isLoading']
    scrollPosition: ['sessions', 'map', '_listScrollPosition']

  clearResults: ->
    pageState
      .set 'building', null
      .set 'companies', null
      .set '_isLoading', false
      .set '_listScrollPosition', null

  pageState: pageState

  render: ->
    companies = @state.session.companies
    building = @state.session.building
    sectionTitle = if building then "Building #{building}" else "Choose building"

    <div className="flex-column">
      <Heading>Interactive map</Heading>

      <div className="interactive-map">
        <div className="interactive-map__map">
          <Map onChoose={onBuildingChoose} selected={building} />
        </div>

        <Section title={sectionTitle} close={@clearResults if companies} />

        <div className="interactive-map__results">
          <CompaniesList companies={companies} msgNothing={false} />
          {<LoadingSpinner stretch /> if @state.isLoading}
        </div>
      </div>
    </div>
