@AboutPage = React.createClass
  displayName: 'AboutPage'
  render: ->
    <div>
      <Heading close="search">About project</Heading>
      <Content>
        <h2>Interactive map</h2>

        <p>Company Amodio (from the latin amodo - from now on) is positioning itself as a furniture design workshop. Our goal - to provide the possibility to use domestic furniture in its production experience of professional industrial designers and design engineers to create competitive furniture. The company management selected long-term strategy of focusing on the market for manufacturers of furniture. This approach to work in the B2B segment, the company has established high internal quality standards. The company provides a full range of services in design and construction of furniture: the choice of stylistic directions until the establishment of the use of the product instructions. In addition, the company offers engineering services in the field of development, rationalization and modernization of the design and manufacturing processes. Workshop AMODO specializes in the design and construction of furniture, interior elements, hotel, restaurant, commercial equipment and POSM. The company's employees are actively involved in the exhibitions and round tables involving issues of industrial design and the ways of its development in the furniture sector. In addition to the engineering activity, the company began to develop in parallel consulting and information services employed in its projects of innovative materials and technologies.</p>

        <h2>Stained glass</h2>

        <p>
          In the production of stained-glass panels for furniture and interior objects The following technologies are used:
          <br />
          <ul>
            <li>art printing technology</li>
            <li>cutting plotter</li>
            <li>facet-film technology</li>
            <li>"Tiffany" technique</li>
          </ul>
        </p>

        <p>
          The following materials are used:
          <br />
          <ul>
            <li>colored glass company «Spectrum Glass Company», USA</li>
            <li>triplex</li>
            <li>hardiness drained away</li>
            <li>polycarbonate</li>
            <li>solid polycarbonate</li>
          </ul>
        </p>
      </Content>
    </div>
