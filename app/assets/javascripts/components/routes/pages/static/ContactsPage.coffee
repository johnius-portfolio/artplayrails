@ContactsPage = React.createClass
  displayName: 'ContactsPage'
  render: ->
    <div>
      <Heading close="search">Contacts</Heading>
      <Content>
        <p>
          <span>Office</span>
          <br />
          <b>+7 (495) 727 09 39</b>
        </p>

        <p>
          <span>Hotline for supporters</span>
          <br />
          <b>+7 (495) 995 00 00</b>
        </p>

        <p>
          <span>Fax</span>
          <br />
          <b>+7 (495) 727 09 38</b>
        </p>

        <p>
          <span>E-mail</span>
          <br />
          <b>russia@wwf.ru</b>
        </p>

        <p>
          <span>Address for correspondence</span>
          <br />
          <b>109240 PO #3, Moscow, World Wildlife Fund</b>
        </p>

        <p>
          <span>Actual address</span>
          <br />
          <b>19 Nikolyamskaya St, Bld 3, Moscow 109240</b>
        </p>
      </Content>
    </div>
