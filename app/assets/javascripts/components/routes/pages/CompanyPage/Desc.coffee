@CompanyDesc = React.createClass
  displayName: 'CompanyDesc'
  contextTypes:
    router: React.PropTypes.func
    company: React.PropTypes.object

  render: ->
    company = Object @context.company

    <div className='company-page__desc'>
      {
        unless @context.company
          <Dummy caption="Loading..." />

        else
          <div className="company-page__desc-brands">
            <img src="/static/Brands.png" />
            <b>Brands</b>
            <p>{company.brands}</p>
          </div> if company.brands

          <div className="company-page__desc-content">
            <Scrollable>
              <div dangerouslySetInnerHTML={{__html: company.body_text}} />
            </Scrollable>
          </div> if company.body_text
      }
    </div>
