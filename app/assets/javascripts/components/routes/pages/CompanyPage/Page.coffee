# State

@AppState.select('sessions').set 'company',
  data: null
  _isLoading: false


# Data

pageState = @AppState.select('sessions', 'company')


# Actions

loadCompany = (id) ->
  pageState
    .set '_isLoading', true

  $.ajax
    url: "/company/#{id}"
    success: (response) ->
      pageState
        .set 'data', response
        .set '_isLoading', false
    error: ->
      pageState.set '_isLoading', false

clearCompany = ->
  pageState
    .set 'data', null
    .set '_isLoading', false


# Class

@CompanyPage = React.createClass
  displayName: 'CompanyPage'
  contextTypes:
    router: React.PropTypes.func
  childContextTypes:
    company: React.PropTypes.object
  getChildContext: ->
    company: @state.company

  mixins: [BaobabReactMixins.branch, ReactRouter.State]
  cursors:
    company: ['sessions', 'company', 'data']
    isLoading: ['sessions', 'company', '_isLoading']

  componentWillMount: ->
    loadCompany @getParams().id

  componentWillReceiveProps: ->
    unless (@state.company || {}).id == parseInt(@getParams().id || 0)
      loadCompany @getParams().id

  componentWillUnmount: ->
    clearCompany()

  render: ->
    company = @state.company || {}
    links = {}
    links.company_location = 'Map'
    links.company_desc = 'Description' if company.body_text
    links.company_gallery = 'Gallery' unless _.isEmpty company.images
    preview_text = company.preview_text
    preview_text &&= preview_text.slice(0, 262) + '...'

    if _.isEmpty company
      <span />

    else
      <div className='company-page'>
        <Section title={company.name} close company />

        <div className='company-page__header'>
          <div className='company-page__header-contacts'>
            <div className="__location">
              <h3 className="__floor">{String(company.location or '').replace('-', '–')}</h3>
              <b className="__floor-caption">Building – Floor</b>
            </div>
            <div className="__contacts">{[company.email, String(company.phone or '').replace(/[,;]/g, '\n').replace(/([0-9])\(/g, '$1 (').replace(/\)([0-9])/g, ') $1')].join('\n').trim()}</div>
          </div>
          <div className='company-page__header-text'>
            {<img src={company.logo} /> if company.logo}
            {<p dangerouslySetInnerHTML={{__html: preview_text}} /> if preview_text}
          </div>
        </div>

        <Navigation links={links} params={{id: @getParams().id}} profile />

        <div className='company-page__content'>
          <RouteHandler />
        </div>
      </div>
