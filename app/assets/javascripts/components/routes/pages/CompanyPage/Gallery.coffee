@CompanyGallery = React.createClass
  displayName: 'CompanyGallery'
  contextTypes:
    router: React.PropTypes.func
    company: React.PropTypes.object

  getImages: ->
    if @context.company and not _.isEmpty @context.company.images
      @context.company.images
    else
      null

  render: ->
    company = @context.company or {}
    images = @getImages()

    <div className='company-page__gallery'>
      {
        <Gallery images={images} /> unless _.isEmpty images
      }
    </div>
