@CompanyMap = React.createClass
  displayName: 'CompanyMap'
  contextTypes:
    router: React.PropTypes.func
    company: React.PropTypes.object
  render: ->
    company = {}

    if company = @context.company
      building = company.location.replace(' ', '').match(/(^[0-9]+)/g)[0]
      tags = company.tags

    <div className="company-page__location">
      <div className='company-page__map'>
        <Map selected={building} />
      </div>
      {<div className="company-page__tags">
        <span>Categories:</span>

        <div className="company-page__tags-cloud">
          <TagsCloud tags={tags} />
        </div>
      </div> unless _.isEmpty company.tags}
    </div>
