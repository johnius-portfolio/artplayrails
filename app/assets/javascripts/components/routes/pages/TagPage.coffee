# State

@AppState.select('sessions').set 'tag',
  tagCategory: null
  tagName: null
  companies: null
  _isLoading: false
  _listScrollPosition: null


# Data

pageState = @AppState.select('sessions', 'tag')


# Actions

ajaxHandler = null

LoadTag = (id) ->
  ajaxHandler.abort() if _.isObject(ajaxHandler) and _.isFunction(ajaxHandler['abort'])

  pageState
    .set '_isLoading', true
    .set '_listScrollPosition', null

  ajaxHandler =
    $.ajax
      url: "/tags/#{id}"
      success: (response) ->
        pageState
          .set 'companies', response.companies
          .set 'tagCategory', response.tag_category.name
          .set 'tagName', response.name
          .set '_isLoading', false
          .set '_listScrollPosition', 0
      error: ->
        pageState.set '_isLoading', false

ClearTag = ->
  pageState
    .set 'tagCategory', null
    .set 'tagName', null
    .set 'companies', null
    .set '_isLoading', false
    .set '_listScrollPosition', null


# Class

@TagPage = React.createClass
  displayName: 'TagPage'
  contextTypes:
    router: React.PropTypes.func
  mixins: [BaobabReactMixins.branch, ReactRouter.State, keepScrollPositionMixin]
  cursors:
    session: ['sessions', 'tag']
    companies: ['sessions', 'tag', 'companies']
    isLoading: ['sessions', 'tag', '_isLoading']
    scrollPosition: ['sessions', 'tag', '_listScrollPosition']
  componentDidMount: ->
    LoadTag @getParams().id
  componentWillUnmount: ->
    ClearTag()
  pageState: pageState
  render: ->
    if @state.session.tagCategory
      <div className="tag-page">
        <Heading simple>{@state.session.tagCategory}</Heading>
        <div className="tag-page__section">
          <Section title={@state.session.tagName} close />
        </div>
        <CompaniesList
          companies  = {@state.companies}
          msgEmpty   = 'No companies for given tag'
          msgNothing = '...'
        />
      </div>
    else
      <span />
