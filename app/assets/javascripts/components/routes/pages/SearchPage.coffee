# State

@AppState.select('sessions').set 'search',
  query: null
  companies: null
  _isLoading: false
  _listScrollPosition: null


# Data

pageState = @AppState.select('sessions', 'search')


# Actions

ajaxHandler = null

Search = (query = '') ->
  ajaxHandler.abort() if _.isObject(ajaxHandler) and _.isFunction(ajaxHandler['abort'])

  pageState
    .set 'query', query
    .set '_isLoading', true
    .set '_listScrollPosition', null

  ajaxHandler =
      $.ajax
        url: "/search/#{query}"
        success: (response) ->
          pageState
            .set '_isLoading', false
            .set '_listScrollPosition', 0
            .set 'companies', response
        error: ->
          pageState
            .set '_isLoading', false


# Custom

initKeyboard = ($input) ->
  $input.keyboard
    language: ['ru_en']
    appendLocally: true
    usePreview: false
    stayOpen: true
    stickyShift: false
    appendTo: $input.get(0)
    autoAccept: true
    caretToEnd: true
    display:
      normal: 'EN'
      meta: 'RU'
      meta1: '12#'
      accept: ' '
      shift: ' '
      b: ' '
    layout : 'custom'
    change: (ev, kv, el) ->
      unless pageState.get('query') == el.value
        Search el.value
    customLayout:
      'default': [
        'q w e r t y u i o p {b}'
        'a s d f g h j k l'
        '{shift} z x c v b n m , . {shift}'
        '{meta1} {meta} {space} {meta1}'
      ]
      'shift': [
        'Q W E R T Y U I O P {b}'
        'A S D F G H J K L'
        '{shift} Z X C V B N M , . {shift}'
        '{meta1} {meta} {space} {meta1}'
      ]
      'meta': [
        'й ц у к е н г ш щ з х {b}'
        'ф ы в а п р о л д ж э'
        '{shift} я ч с м и т ь б ю ъ {shift}'
        '{meta1} {normal} {space} {meta1}'
      ]
      'meta-shift': [
        'Й Ц У К Е Н Г Ш Щ З Х {b}'
        'Ф Ы В А П Р О Л Д Ж Э'
        '{shift} Я Ч С М И Т Ь Б Ю Ъ {shift}'
        '{meta1} {normal} {space} {meta1}'
      ]
      'meta1': [
        '1 2 3 4 5 6 7 8 9 0 {b}'
        '- : ; ( ) & @'
        '_ , . ? ! \' "'
        '{meta} {normal} {space} {meta}'
      ]


# Class

@SearchPage = React.createClass
  displayName: 'SearchPage'
  mixins: [BaobabReactMixins.branch, keepScrollPositionMixin]
  cursors:
    session: ['sessions', 'search']
    companies: ['sessions', 'search', 'companies']
    isLoading: ['sessions', 'search', '_isLoading']
    scrollPosition: ['sessions', 'search', '_listScrollPosition']

  componentDidMount: ->
    $input = $(@refs.input.getDOMNode())
    keyboard = initKeyboard($input)

    unless @state.companies
      Search()

  pageState: pageState

  render: ->
    companies = @state.companies
    query = @state.session.query

    <div className="search-page">
      <Heading simple>Search ARTPLAY</Heading>
      <div className="search-page__searchbar" data-search>
        <input type="text" defaultValue={query} ref="input" />
      </div>
      <div className="search-page__results">
        <CompaniesList companies={companies} msgEmpty="No matches found" />
        {<LoadingSpinner stretch /> if @state.isLoading}
      </div>
    </div>
