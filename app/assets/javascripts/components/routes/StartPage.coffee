# State

@AppState.select('sessions').set 'start',
  adShown: false


# Data

pageState = @AppState.select('sessions', 'start')


# Class

@StartPage = React.createClass
  displayName: 'StartPage'
  mixins: [BaobabReactMixins.branch]
  cursors:
    adShown: ['sessions', 'start', 'adShown']

  handleVideoClick: ->
    pageState.set('adShown', false)

  render: ->
    <div className="layout__inner-start">
      {<VideoBanner full onClick={@handleVideoClick} /> if pageState.get('adShown')}
      <div className="layout__start">
        <div className="start-page">
          <div className="start-page__heading">
            <div className="start-page__logo" />
            <div className="start-page__title">Interactive terminal</div>
          </div>
          <Link to="search" className="start-page__start">Enter</Link>
          <div className="start-page__map">
            <Map guide />
          </div>
        </div>
      </div>
      <footer className="layout__footer">
        <Navigation links={{about: 'About', adverts: 'Advertisement', contacts: 'Contacts'}} footer />
      </footer>
    </div>
