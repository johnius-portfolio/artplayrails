@Navigation = React.createClass
  displayName: 'Navigation'
  getDefaultProps: ->
    links: []
    footer: null
    profile: null
    params: null
  render: ->
    classes = classNames
      'navigation': true
      '_footer': @props.footer
      '_profile': @props.profile

    <nav className={classes}>
      {
        if @props.links and _.size @props.links
          unless @props.profile
            <div className="navigation__links">
              {
                for url, title of @props.links
                  <Link to={url} key={url}>
                    <span>{title}</span>
                  </Link>
              }
            </div>
          else
            <div className="navigation__links">
              {
                for url, title of @props.links
                  <ReplaceLink to={url} key={url} params={@props.params}>
                    <span>{title}</span>
                  </ReplaceLink>
              }
            </div>
      }
    </nav>
