Router = @ReactRouter
@Route = Router.Route
@DefaultRoute = Router.DefaultRoute
@RouteHandler = Router.RouteHandler
@NotFoundRoute = Router.NotFoundRoute
@Link = Router.Link


# Application layout

Layout = React.createClass
  displayName: 'Layout'
  mixins: [ReactRouter.Navigation, BaobabReactMixins.root]
  cursors:
    adShown: ['sessions', 'start', 'adShown']

  startTimer: ->
    clearTimeout @clickTimeout
    @clickTimeout = setTimeout =>
      AppState.set(['sessions', 'start', 'adShown'], true)
      @transitionTo 'start'
    , 60000

  clickTimeout: false

  componentDidMount: ->
    # @startTimer()
    # $(document).off 'mouseup.advert'
    # $(document).on 'mouseup.advert', @startTimer

  render: ->
    <div className="layout">
      <RouteHandler />
    </div>

Pages = React.createClass
  displayName: 'PagesRoute'
  render: ->
    <div className="layout__inner-page">
      <aside className="layout__sidebar">
        <Sidebar />
      </aside>

      <main className="layout__body">
        <header className="layout__header">
          <Navigation links={{search: 'Search ARTPLAY', tags: 'Services', map: 'Interactive map'}} />
        </header>
        <section className="layout__content">
          <RouteHandler />
        </section>
        <footer className="layout__footer">
          <Navigation links={{about: 'About', adverts: 'Advertisement', contacts: 'Contacts'}} footer />
        </footer>
      </main>
    </div>


# Routes

routes =
  <Route handler={Layout}>
    <Route name="start" path="/" handler={StartPage} />

    <Route handler={Pages}>
      <Route name="search" handler={SearchPage} />
      <Route name="tags" handler={TagsPage} />
      <Route name="tag" path="tag/:id" handler={TagPage} />
      <Route name="map" handler={MapPage} />

      <Route name="company" path="company/:id" handler={CompanyPage}>
        <Route name="company_location" handler={CompanyMap} />
        <Route name="company_desc" handler={CompanyDesc} />
        <Route name="company_gallery" handler={CompanyGallery} />

        <DefaultRoute handler={CompanyMap} />
      </Route>

      <Route name="static">
        <Route name="about" handler={AboutPage} />
        <Route name="adverts" handler={AdPage} />
        <Route name="contacts" handler={ContactsPage} />

        <DefaultRoute handler={AboutPage} />
      </Route>
    </Route>

    <NotFoundRoute handler={StartPage} />
    <DefaultRoute handler={StartPage} />
  </Route>


# Create routes

$ ->
  Router.run routes, (Handler) ->
    React.render <Handler tree={AppState} />, document.body
