require File.join(Rails.root, 'lib/remoteimage')

class MainController < ApplicationController
  def index
  end

  def search
    if (query = params[:id])
      company = JSON[Company.search_by_name(query).select('id, name, logo').limit(15).to_json]
    else
      company = JSON[Company.all.select('id, name, logo').order('name').to_json]
    end

    company.each do |item|
      if item['logo']
        item['logo'].gsub! 'http://www.artplay.ru', '.'
        item['logo'] = nil unless exists(item['logo'])
      end
    end if !!ENV['INTERNAL_ASSETS']

    company = JSON.pretty_generate(company) if params.has_key? :pretty

    render json: company
  end

  def building
    building = JSON[Company
      .where("location LIKE :prefix", prefix: "#{params[:id]}-%")
      .select('id, name, logo').to_json]

    building.each do |item|
      if item['logo']
        item['logo'].gsub! 'http://www.artplay.ru', '.'
        item['logo'] = nil unless exists(item['logo'])
      end
    end if !!ENV['INTERNAL_ASSETS']

    building = JSON.pretty_generate building if params.has_key? :pretty

    render json: building
  end

  def company
    company = Company.find(params[:id]) rescue {}

    company = Hashie::Mash.new JSON[company.to_json(
      :include => [
        {
          :tags => {
            :include => {
              :tag_category => { only: [:name] }
            },
            :only => [:id, :name]
          }
        },
        {
          :images => {
            :only => [:url]
          }
        }
      ]
    )]

    if company.any?
      company.tags.each do |tag|
        tag.tag_category = tag.tag_category.name
      end if company.tags and company.tags.any?

      company.images.map! do |image|
        if !!ENV['INTERNAL_ASSETS']
          imagepath = RemoteImagePath.new image.url
          filepath = imagepath.get_filepath
          previewpath = imagepath.get_previewpath
          path = File.exist?(File.join(Rails.public_path, previewpath)) ?
            previewpath : filepath
          local_path = File.join(Rails.public_path, path)

          if File.exist? local_path
            imagefile = MiniMagick::Image.open local_path
            dims = imagefile.dimensions

            {
              url: path,
              dims: dims
            }
          end
        else
          {
            url: image.url,
            dims: []
          }
        end
      end.compact! if company.images and company.images.any?


      if !!ENV['INTERNAL_ASSETS']
        remove_domain!(company.logo)
        remove_domain!(company.mainpic)

        company.logo = nil unless exists(company.logo)
      end
    end

    company = JSON.pretty_generate(company) if params.has_key? :pretty

    render json: company
  end

  def categories
    category = TagCategory.all rescue {}

    category = JSON[category.to_json(
      :include => [{
        :tags => {
          :only => [:id, :name]
        }
      }]
    )]

    category.map! do |cat|
      cat = Hashie::Mash.new cat
    end

    category = JSON.pretty_generate(category) if params.has_key? :pretty

    render json: category
  end

  def tags
    companies = Tag.find(params[:id]) rescue {}

    companies = JSON[companies.to_json(
      :include => [
        {
          :tag_category => {
            :only => [:name]
          }
        },
        {
          :companies => {
            :only => [:id, :name, :logo]
          }
        }
      ],
      :only => [:id, :name]
    )]

    companies['companies'].each do |company|
      if company['logo']
        company['logo'].gsub! 'http://www.artplay.ru', '.'
        company['logo'] = nil unless exists(company['logo'])
      end
    end if !!ENV['INTERNAL_ASSETS']

    companies = JSON.pretty_generate(companies) if params.has_key? :pretty

    render json: companies
  end


  # Links of pictures to download

  def pictures
    images = Image.all.pluck(:url)
    logos = Company.all.pluck(:logo, :mainpic).flatten.reject(&:blank?)
    pictures = (images + logos).uniq

    if params.has_key? :length
      render text: pictures.length
    else
      render partial: 'pictures', locals: { pictures: pictures }
    end
  end

  private

  def remove_domain url
    if url
      url.gsub('http://www.artplay.ru', '.')
    else
      nil
    end
  end

  def remove_domain! url
    url.gsub!('http://www.artplay.ru', '.') if url
  end

  def exists path
    if path.is_a? String
      File.exists? Rails.root.join('public', Pathname.new(path))
    else
      false
    end
  end
end
