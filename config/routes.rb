Rails.application.routes.draw do
  resources :adverts
  root 'main#index'
  get '/:action(/:id)', controller: 'main'
end
