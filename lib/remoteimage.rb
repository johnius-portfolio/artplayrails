require 'uri'
require 'pathname'

class RemoteImagePath
  def initialize url, base = nil
    @url = URI(url)
    @base = base
  end

  def get_url
    @url
  end

  def get_filepath
    if @base
      File.join(@base, @url.path[1..-1])
    else
      @url.path[1..-1]
    end
  end

  def get_dirpath
    File.dirname get_filepath
  end

  def get_filename extension = true
    suffix = '.*' unless extension

    File.basename get_filepath, (suffix or '')
  end

  def get_extname
    File.extname get_filepath
  end

  def get_previewpath
    File.join get_dirpath, get_previewname
  end

  def get_previewname
    [get_filename(false), '@@preview', get_extname].join
  end
end