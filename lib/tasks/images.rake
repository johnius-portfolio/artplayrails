require 'uri'
require 'pathname'
require 'fileutils'
require 'net/http'
require_relative '../remoteimage'

namespace :images do
  desc "Download images from http://artplay.ru to public directory. Skip existing images"
  task download: :environment do
    # Grab images from db
    images = Image.all.pluck(:url)
    logos = Company.all.pluck(:logo, :mainpic).flatten
    urls = (images + logos).uniq.compact

    # Create images queue for downloading
    queue = Queue.new
    urls.map { |url| queue << url }

    # Log mutexes
    log_mutex = Mutex.new
    mkpath_mutex = Mutex.new

    threads = 5.times.map do
      Thread.new do
        Net::HTTP.start('www.artplay.ru', 80) do |http|
          while !queue.empty? && url = queue.pop
            imagepath = RemoteImagePath.new url, Rails.public_path

            # Creating folder structure recursively if not exist
            mkpath_mutex.synchronize do
              unless File.exist? dir = imagepath.get_dirpath
                puts "Creating #{dir}"
                FileUtils.mkpath dir
              end
            end

            # Saving file
            unless File.exist? filepath = imagepath.get_filepath
              url = imagepath.get_url
              request = Net::HTTP::Get.new(url)
              response = http.request request

              if response.code.to_i == 200
                log_mutex.synchronize do
                  puts "Saving file #{imagepath.get_filename}"
                end

                File.open(filepath, 'wb').write response.body
              else
                log_mutex.synchronize do
                  puts "File not found #{imagepath.get_filename}"
                end
              end
            else
              log_mutex.synchronize do
                puts "Skipping file #{imagepath.get_filename}"
              end
            end
          end
        end
      end
    end

    threads.each(&:join)
  end

  desc "Generate previews for images if their size bigger than needed"
  task previews: :environment do
    images = Image.all.pluck(:url).uniq.compact

    while !images.blank? && url = images.shift
      imagepath = RemoteImagePath.new url, Rails.public_path
      filepath = imagepath.get_filepath

      if File.exist?(filepath) and File.size(filepath) > 0
        image = MiniMagick::Image.open filepath

        if File.exist? imagepath.get_previewpath
          puts "Preview exists (#{imagepath.get_previewname})"
        else
          if image.width > 845 or image.height > 640
            puts "Generating preview (#{image.dimensions.join 'x'}, "\
                 "#{imagepath.get_filename})"
            image.resize '845x640'
            image.write imagepath.get_previewpath
          else
            puts "Image matches requirements (#{imagepath.get_filename})"
          end
        end

      else
        puts "Image doesn't exist"
      end
    end
  end

end
