## Downloading pictures

Pictures — either logos, main pictures and gallery images

**Download all images**
*Run from ./public/*
<!-- nc - no-clobber, do not download images which already exists -->
<!-- nH - do not use address as a root of images when downloading recursively -->
<!-- r - download recursively, save hierarchy of folders -->
<!-- i - download files from list -->
curl http://127.0.0.1:3000/images -o - | wget -nH -r -nc -i -

**Check number of pictures which was responded with error**
*Run from ./public/*
curl http://127.0.0.1:3000/pictures -o - | wget -nH -r -nc -i - 2>&1 | grep ERROR | wc -l

**Check number of unique pictures in database**
curl http://127.0.0.1:3000/pictures?length

**Check number of downloaded images**
*Run from ./public/*
<!-- type -f - list only files (not folders) -->
find . -type -f | wc -l

**To use local pictures**
*Run server using this command*
INTERNAL_ASSETS=1 rails s

**Generate preview images**
*Run in public/videos*
for vid in *.mp4; do
  ffmpeg -y -i $vid -vframes 1 -f image2 $vid.jpg
done