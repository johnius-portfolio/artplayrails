class CreateAdverts < ActiveRecord::Migration
  def change
    create_table :adverts do |t|
      t.string :filename
      t.references :company, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
