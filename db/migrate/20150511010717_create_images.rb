class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.references :company, index: true, foreign_key: true
      t.string :url
    end
  end
end
