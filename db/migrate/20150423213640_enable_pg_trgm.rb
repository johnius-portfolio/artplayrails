class EnablePgTrgm < ActiveRecord::Migration
  def change
    execute 'CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA pg_catalog;'
  end
end
