class CreateCompaniesTags < ActiveRecord::Migration
  def change
    create_table :companies_tags do |t|
      t.references :company, index: true, foreign_key: true
      t.references :tag, index: true, foreign_key: true
    end
  end
end
