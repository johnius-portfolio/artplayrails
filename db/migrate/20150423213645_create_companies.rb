class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :logo
      t.string :mainpic
      t.string :link_site
      t.string :phone
      t.string :email
      t.string :preview_text
      t.text :body_text
      t.string :location
      t.text :brands
    end

    execute "CREATE INDEX company_name_trgm_idx ON companies USING gist (name gist_trgm_ops);"
  end
end
