# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require './db/parser'
require 'pry'

ActiveRecord::Base.establish_connection


STDOUT.puts 'Parsing json...'
companies = ParseCompanies.new


STDOUT.puts 'Truncating all tables...'
ActiveRecord::Base.connection.tables.each do |table|
  next if table == 'schema_migrations'

  ActiveRecord::Base.connection.execute("TRUNCATE #{table} CASCADE")
end


STDOUT.puts 'Migrating companies...'
ActiveRecord::Base.transaction do
  companies.list.each do |company|
    company[:name] = CGI.unescapeHTML(company[:name]).strip
    Company.create company
  end
end


STDOUT.puts 'Migrating companies images...'
ActiveRecord::Base.transaction do
  companies.images.each do |image|
    Image.create image
  end
end


STDOUT.puts 'Migrating categories...'
ActiveRecord::Base.connection.execute "INSERT INTO tag_categories (name) VALUES ('Товары и сервисы'), ('Архитектура и дизайн')"


STDOUT.puts 'Migrating unique tags...'
ActiveRecord::Base.transaction do
  categories = JSON.parse File.read './db/categories.json'
  companies.uniqueTags.each do |tag|
    tag[:tag_category_id] = categories[tag[:name]]

    Tag.create tag
  end
end


STDOUT.puts 'Migrating companies tags...'
companiesTags = []

companies.tags.each do |item|
  companiesTags.push "(#{item[:company_id]}, #{item[:tag_id]})"
end

ActiveRecord::Base.connection.execute "INSERT INTO companies_tags (company_id, tag_id) VALUES #{companiesTags.join ', '}"
