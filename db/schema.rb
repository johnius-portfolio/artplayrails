# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150616071012) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"

  create_table "adverts", force: :cascade do |t|
    t.string   "filename"
    t.integer  "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "adverts", ["company_id"], name: "index_adverts_on_company_id", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "logo"
    t.string "mainpic"
    t.string "link_site"
    t.string "phone"
    t.string "email"
    t.string "preview_text"
    t.text   "body_text"
    t.string "location"
    t.text   "brands"
  end

  add_index "companies", ["name"], name: "company_name_trgm_idx", using: :gist

  create_table "companies_tags", force: :cascade do |t|
    t.integer "company_id"
    t.integer "tag_id"
  end

  add_index "companies_tags", ["company_id"], name: "index_companies_tags_on_company_id", using: :btree
  add_index "companies_tags", ["tag_id"], name: "index_companies_tags_on_tag_id", using: :btree

  create_table "images", force: :cascade do |t|
    t.integer "company_id"
    t.string  "url"
  end

  add_index "images", ["company_id"], name: "index_images_on_company_id", using: :btree

  create_table "tag_categories", force: :cascade do |t|
    t.string "name"
  end

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "tag_category_id", null: false
  end

  add_index "tags", ["tag_category_id"], name: "index_tags_on_tag_category_id", using: :btree

  add_foreign_key "adverts", "companies"
  add_foreign_key "companies_tags", "companies"
  add_foreign_key "companies_tags", "tags"
  add_foreign_key "images", "companies"
  add_foreign_key "tags", "tag_categories"
end
