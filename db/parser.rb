require 'open-uri'
require 'json'
require 'pry'
require 'hashie'


JSON_FILENAME = 'db/artplay.json'

class ParseCompanies
  def initialize
    companies = JSON.parse(File.read JSON_FILENAME)['nodes']

    companies.map! do |company|
      company = company['node']
      company['id'] = company['id'].to_i
      company['tags'] = parseTags company['tags']

      %w(preview_text body_text).each do |column|
        if company[column]
          company[column] = unifyText company[column]
        end
      end

      Hashie::Mash.new company
    end

    @companies = companies
    @uniqueTags = getUniqueTags
  end

  # Get unique tags
  def uniqueTags
    @uniqueTags.map.with_index do |tag, index|
      {id: index, name: tag}
    end
  end

  # Get tags of each company
  def tags
    @companies.map do |company|
      company.tags.map! do |tag|
        {company_id: company.id, tag_id: @uniqueTags.index(tag)}
      end
    end.flatten
  end

  # Get images of each company
  def images
    @companies.map do |company|
      next unless company.images

      company.images.map do |image|
        {company_id: company.id, url: image.pop}
      end
    end.flatten
  end

  # Get list of companies
  def list
    @companies.map do |company|
      company = company.inject({}) { |memo, (k,v)| memo[k.to_sym] = v; memo }
      company.except :images, :tags
    end
  end

  private

  def parseTags tags
    tags
      .gsub(', еще в ARTPLAY', '')
      .scan(/([А-Я][^А-Я]+)(?:,|$)/)
      .flatten
  end

  def getUniqueTags
    unique = []

    @companies.map do |company|
      company.tags.map do |tag|
        unique << tag unless unique.include? tag
      end
    end

    unique.sort
  end

  def unifyText text
    text.gsub("&nbsp;", " ").strip.gsub("'", '&#039;').gsub("\n", "<br />")
  end
end
